### Title
    Installed Packages and Advertising ID Information Cordova plugin

#### Description
    This plugin returns
    1. a information list of installed apps on the mobile system.
    2. google advertising id of device

#### Supported Platform
    - Android

#### Installation
    cordova plugin add "path/cordova-plugin-pkdeviceinfo"
    or 
    cordova plugin add https://SmartMan21Cen@bitbucket.org/SmartMan21Cen/cordova-plugin-pkdeviceinfo.git

#### Example
    declare const cordova: any;

    if (cordova.plugins.pkdevice) {

        cordova.plugins.pkdevice.getApplications((res) => {
          console.log('installed applications', res);
        }, (err) => {
          console.error('Error:', err);
        });

        cordova.plugins.pkdevice.getAdvertiseID((res) => {
          console.log('advertising id', res);
        }, (err) => {
          console.error('Error:', err);
        })

    }

#### Parameters
    - getApplications - successCallback (Function)
        Success Callback Receive
        * Return: JSONArray
        * Type Return Example:  
        ["10001;/data/data/com.sec.android.gallery3d;com.sec.android.gallery3d"] 
        Uid;DataDir;PackageName

    - getApplications - errorCallback (Function)
        Error Callback Receive

    - getAdvertiseID - successCallback (Function)
        * Return: String
        * Example: ********-****-****-****-************
