package cordova.plugin.pkdeviceinfo;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Build;
import android.os.AsyncTask;
import android.util.Log;

import org.apache.cordova.PluginResult;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;

/**
 * This class echoes a string called from JavaScript.
 */
public class pkdevice extends CordovaPlugin {

    public Context context = null;
    private static final boolean IS_AT_LEAST_LOLLIPOP = Build.VERSION.SDK_INT >= 21;
    private String _maid;

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        
        context = IS_AT_LEAST_LOLLIPOP ? cordova.getActivity().getWindow().getContext() : cordova.getActivity().getApplicationContext();

        if (action.equals("getApplications")) {
            JSONArray resultArray = new JSONArray();

            final PackageManager pm = cordova.getActivity().getPackageManager();
            Intent intent = new Intent(Intent.ACTION_MAIN, null);
            intent.addCategory(Intent.CATEGORY_LAUNCHER);
            List<ResolveInfo> apps = pm.queryIntentActivities(intent, PackageManager.GET_META_DATA);
            for (ResolveInfo packageInfo : apps) {
                JSONObject result = getPackageInfo(context, packageInfo.activityInfo.applicationInfo.packageName);
                resultArray.put(result);
            }
            this.getApplications(resultArray, callbackContext);
            return true;
        } else if (action.equals("getAdvertiseID")) {

            AdvertisingIdClient.Info idInfo = null;
            try {
                idInfo = AdvertisingIdClient.getAdvertisingIdInfo(context.getApplicationContext());
            } catch (GooglePlayServicesNotAvailableException e) {
                e.printStackTrace();
            } catch (GooglePlayServicesRepairableException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            String advertId = null;
            try {
                advertId = idInfo.getId();
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
            Log.e("advertId", advertId);

            this.getAdvertiseID(advertId, callbackContext);
            return true;
            
        }
        return false;
    }

    private static JSONObject getPackageInfo(Context context, String packageName) {
        JSONObject jsonObject = new JSONObject();
        PackageManager pm = context.getPackageManager();
        try {
            PackageInfo packageInfo = pm.getPackageInfo(packageName, 0);
             SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

             jsonObject.put("id", packageName);
             jsonObject.put("name", packageInfo.applicationInfo.loadLabel(pm).toString());
             jsonObject.put("versionCode", packageInfo.versionCode);
             jsonObject.put("versionName", packageInfo.versionName);
             jsonObject.put("installedDate", sdf.format(new Date(packageInfo.firstInstallTime)));
             jsonObject.put("lastUsed", sdf.format(new Date(packageInfo.lastUpdateTime)));

             File file = new File(packageInfo.applicationInfo.sourceDir);
             jsonObject.put("memoryUsedMb", file.length() / 1024.00 / 1024.00);
        } catch (PackageManager.NameNotFoundException | JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    private void getApplications(JSONArray items, CallbackContext callbackContext) {
        if (items != null && items.length() > 0) {
            callbackContext.success(items);
        } else {
            callbackContext.error("App can't get applications information.");
        }
    }

    private void getAdvertiseID(String adid, CallbackContext callbackContext) {
        if (adid != null && adid.length() > 0) {
            callbackContext.success(adid);
        } else {
            callbackContext.error("App can't get application advertising id.");
        }
    }

}
