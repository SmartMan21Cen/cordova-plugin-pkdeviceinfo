var exec = require('cordova/exec');

exports.getApplications = function (success, error) {
    exec(success, error, 'pkdevice', 'getApplications', null);
};

exports.getAdvertiseID = function (success, error) {
    exec(success, error, 'pkdevice', 'getAdvertiseID', null);
};
